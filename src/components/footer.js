import React from 'react'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { Icon,  } from '@ant-design/react-native';
import { View } from "react-native";
import {
    FooterContainer,
    FooterIcon, NewICon
} from '../components/General.styled'

class Footer extends React.Component {
    render() {
        const { goToProfile, goToWallet, goToHome } = this.props
        return (
            <FooterContainer >
                <FooterIcon onPress={goToHome} >
                     <NewICon name="home" size='md' />
                </FooterIcon>
                <FooterIcon onPress={goToWallet} >
                    <NewICon   name="wallet" size='md'/>
                </FooterIcon>
                <FooterIcon onPress={goToProfile} >
                    <NewICon   name="user" size='md'/>
                </FooterIcon>
            </FooterContainer>
        )
    }
}

export default connect(
    null,
    dispatch => ({
        goToHome: state => dispatch(push('/summary')),
        goToWallet: state => dispatch(push('/wallets')),
        goToProfile: state => dispatch(push('/profile')),
    })
)(Footer)
