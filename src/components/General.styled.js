import styled from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native';

// general 
export const Center = styled.View`
    align-items: center;
    justify-content: center;
`
export const Container = styled.View`
    flex: 1;    
    align-items: center;
`
export const SimpleContainer = styled.View`
    flex: 1;    
`
export const FullWImage = styled.Image`
    width: 100%;
    height: 100%;
`
export const Container2 = styled.View`
    flex: 1;    
    marginLeft: 5%;
    marginRight: 5%;
    marginTop: 10%
    backgroundColor: white;
    flex-direction: column
`
export const Container3 = styled.View`
    flex: 1;    
    marginLeft: 5%;
    marginRight: 5%;
    flex-direction: column
   
`
export const Container4 = styled.View`
backgroundColor: white; 
marginBottom: 10%;
marginTop: 10%;
height: 350; 
width: 90%;
marginLeft: 5%;
marginRight: 5%;
   
`
export const IconFrame = styled.View`
    width: 35;
    height: 35;    
`

// Login
export const LogoFrame = styled.View`
    width: 200;
    height: 200;
    marginTop: 50;
    
`
export const LoginForm = styled.View`
    width: 80%; 
    height: 40%; 
    marginTop: 80;
    borderRadius: 20; 
    flex-direction: column; 
    paddingRight: 15;
`
export const Column = styled.View`
   
    flex-direction: column; 
   
`
export const Row = styled.View`
   
    flex-direction: row; 
   
`

export const UserInput = styled(InputItem)`
   
    color: white; 
`
export const SubmitButton = styled(Button)`
    width: 40%;
`
export const SimpleText = styled.Text`
    color: white; 
    fontSize: 18;
    textAlign: center;
    marginTop: 10;
`
export const Title = styled.Text`
    color: white; 
    fontSize: 36; 
    textAlign: center;
    marginTop: 5%;
    
`
export const SubTitle = styled.Text`
    fontSize: 28; 
    textAlign: center;
    marginTop: 5%;
    
`
export const FooterIcon = styled.TouchableOpacity`
    flex: 1
    padding-left: 8;
    align-items: center;
    padding-right: 8;
`
export const FooterContainer = styled.View`
    flex-direction: row;
    backgroundColor: #f2f2f2
    padding-top: 5;
    padding-bottom: 5;
    align-items: center;
    height: 50
`
export const NewICon = styled(Icon)`
   color: #00cc99;
   
`