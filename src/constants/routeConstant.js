// Constants for using in router

export const ROUTE_TO_LOGIN = '/'
export const ROUTE_TO_REGISTER = '/register'
export const ROUTE_TO_ADD_WALLET = '/addWallet'
export const ROUTE_TO_SUMMARY = '/summary'
export const ROUTE_TO_EDIT_WALLET = '/editWallet'
export const ROUTE_TO_WALLTETS = '/wallets'
export const ROUTE_TO_PROFILE = '/profile'
export const ROUTE_TO_WALLET_DETAIL = '/walletDetail'

