import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput, Button, ImageBackground, Alert } from 'react-native';
import axios from 'axios';
import {
    Container, Title, SubmitButton, LogoFrame, FullWImage, UserInput,
    SimpleText
} from "../components/General.styled";
import { images } from "../utilities";

import { WhiteSpace, } from '@ant-design/react-native';

class RegisterPage extends Component {
    state = {
        username: '',
        password: '',
        confirmPass: '',
        firstname: '',
        lastname: '',
        isCorrect: false
    }

    changeValue = (state, value) => this.setState({ [state]: value })


    onPressRegister = () => {
        axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
            email: this.state.username, password: this.state.password,
            firstName: this.state.firstname, lastName: this.state.lastname
        }).then(() => {
            Alert.alert(`Success`,
                'Register',
                [
                    { text: 'OK', onPress: () => this.props.backToLogin() },
                ],
                { cancelable: false })

        }).catch(function (error) {
            console.log('error: ', error.response.data.errors)
            Alert.alert(`Error`,
                `Please, check again`,
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false })
        });
    }


    render() {
        console.log('Props: ', this.props)
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg}>

                <Container style={{ margin: '5%', backgroundColor: 'white' }}>

                    <LogoFrame><FullWImage style={{ borderRadius: 120 }} source={images.logo} /></LogoFrame>

                    <UserInput placeholderTextColor='#cccccc' clear
                        placeholder='Username' value={this.state.username}
                        style={{ color: 'black' }}
                        onChangeText={value => this.changeValue('username', value)} ></UserInput>

                    <UserInput placeholderTextColor='#cccccc' clear placeholder='Password' value={this.state.password}
                        style={{ color: 'black' }}
                        type="password"
                        onChangeText={value => this.changeValue('password', value)} ></UserInput>

                    <UserInput placeholderTextColor='#cccccc' clear placeholder='Confirm pass'
                        value={this.state.confirmPass} style={{ color: 'black' }}
                        type="password"
                        onChangeText={value => this.changeValue('confirmPass', value)} ></UserInput>

                    <UserInput placeholderTextColor='#cccccc' clear placeholder='Firstname'
                        value={this.state.firstname} style={{ color: 'black' }}

                        onChangeText={value => this.changeValue('firstname', value)} ></UserInput>

                    <UserInput placeholderTextColor='#cccccc' clear placeholder='Lastname'
                        value={this.state.lastname} style={{ color: 'black' }}
                        onChangeText={value => this.changeValue('lastname', value)} last></UserInput>
                    <WhiteSpace />
                    <SubmitButton onPress={this.onPressRegister} style={{
                        backgroundColor: '#ffcb05',
                        marginTop: 30
                    }} >
                        <SimpleText>Register</SimpleText>
                    </SubmitButton>
                    <TouchableOpacity onPress={ this.props.backToLogin}>
                        <SimpleText style={{color: '#38b6ff'}}>Back to login</SimpleText>
                    </TouchableOpacity>
                </Container>
            </ImageBackground>
        )
    }
}
export default RegisterPage