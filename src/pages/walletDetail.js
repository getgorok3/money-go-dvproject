import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ImageBackground, ScrollView } from 'react-native';
import { images } from "../utilities";
import {
    SimpleContainer, Title, SubmitButton, Container3, IconFrame, FullWImage,
    SimpleText,
    SubTitle,
    Column
} from "../components/General.styled";
import { Card, WhiteSpace, WingBlank, Icon, Badge, List } from '@ant-design/react-native';
import Footer from '../components/footer'


class WalletPageDetail extends Component {
    state = {
        today: new Date(),
        index: null,
        walletName: '',
        money: 0,
        newAmountOfMoney: '',
        topic: '',
        type: '',
        transaction: []
    }
    componentDidMount() {
        this.getDefaultValue()


    }
    getDefaultValue = () => {
        const { item } = this.props
        let data = this.props.transaction
        if (data !== []) {
            data = this.props.transaction.filter(function (each) {
                console.log('a',item.transaction)
                console.log('b',item)
                return each.transaction.walletName === item.walletName
            })
        }
        console.log('data', data)

        this.setState({
            walletName: item,
            index: item.index,
            money: item.money,
            transaction: data,
        })
      




        this.setState({ ...item, transaction: data })
    }
    goToEditWallet = () => {
        const { goToEditWallet } = this.props
        let sentItem = {
            index: this.props.index,
            walletName: this.state.walletName,
            money: this.setState.money
        }
        goToEditWallet(sentItem)

    }
    renderItem = ({ item, index }) => {
        return (

            <Card full style={{ marginBottom: 20 }}>
                <Card.Header
                    title={
                        <View style={{ height: 80, }}>
                            <Text style={{ marginLeft: 16 }}> Wallet:  {item.transaction.walletName}</Text>
                            <Text style={{ marginLeft: 16 }}> Type: {item.transaction.type} </Text>
                            <Text style={{ marginLeft: 16 }}>the amount of money:  {item.transaction.newAmountOfMoney}</Text>
                            <Text style={{ marginLeft: 16 }}> Balance: {item.transaction.money}</Text>


                        </View>
                    }
                    extra={this.state.date}
                >

                </Card.Header>
            </Card>

        )
    }


    render() {
        console.log('render: ', this.state)
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg}>

                <SimpleContainer style={{
                    marginTop: '10%',
                    backgroundColor: 'white'
                }}>
                    <Title style={{ color: 'black' }}>The Wallet Detail</Title>
                    <WhiteSpace />
                    <Card style={{ backgroundColor: '#e6e6ff' }}>
                        <Card.Body style={{ flexDirection: 'column', justifyContent: 'center' }}>
                            <SimpleText style={{ color: 'black' }}>Wallet's name : {this.state.walletName}</SimpleText>
                            <SimpleText style={{ color: 'black' }}>Cash : {this.state.money}</SimpleText>
                        </Card.Body>
                        <Card.Footer>

                        </Card.Footer>
                    </Card>
                    <WhiteSpace />
                    <ScrollView style={{ flex: 1, backgroundColor: '#f2f2f2' }}>
                        {this.state.transaction && this.state.transaction !== [] ? (
                            <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                data={this.state.transaction}
                                renderItem={this.renderItem}
                                extraData={this.state}
                            />
                        ) : (
                            <View></View>
                        )}


                    </ScrollView>
                    <WhiteSpace />
                    <View style={{ justifyContent: 'flex-end' }}>
                        <SubmitButton onPress={this.goToEditWallet}
                            style={{ marginRight: '20%', marginLeft: '20%', width: '60%' }}>
                            <SimpleText style={{ color: '#ffcc00', fontWeight: 'bold' }}>Add transaction</SimpleText>
                        </SubmitButton>
                    </View>
                </SimpleContainer>

                <Footer ></Footer>
            </ImageBackground>
        )
    }

}
export default WalletPageDetail