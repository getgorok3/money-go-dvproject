// Adding new wallet 

import React, { Component } from 'react';

import { View, TouchableOpacity, Text, TextInput, Button, ImageBackground, Alert } from 'react-native';
import axios from 'axios';
import {
    Container, Title, SubmitButton, LogoFrame, FullWImage, UserInput,
    SimpleText
} from "../components/General.styled";
import { images } from "../utilities";

import { WhiteSpace, } from '@ant-design/react-native';
class AddWalletPage extends Component {
    state = {
        walletName: '',
        money: ''
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    addWallet = () => {
        const amountOfMoney = parseInt(this.state.money, 10)
        let wallet = {
            walletName: this.state.walletName,
            money: amountOfMoney
        }
        this.props.addTheWallet(wallet)
    }

    render() {
        console.log('Add wallet ', this.props)
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg}>
                <Container style={{ margin: '5%', backgroundColor: 'white' }}>
                    <LogoFrame><FullWImage  source={images.wallet1} /></LogoFrame>
                    <WhiteSpace />
                    <Title style={{color: 'black'}}>Add a new wallet</Title>

                    <UserInput placeholderTextColor='#cccccc' clear
                        placeholder='Wallet name' value={this.state.walletName}
                        style={{ color: 'black' }}
                        onChangeText={value => this.changeValue('walletName', value)} ></UserInput>

                    <UserInput placeholderTextColor='#cccccc' clear placeholder='Money' value={this.state.money}
                        style={{ color: 'black' }}
                        keyboardType="number"
                        onChangeText={value => this.changeValue('money', value)} ></UserInput>
                    <WhiteSpace />
                    <SubmitButton onPress={this.addWallet} style={{
                        backgroundColor: '#ffcb05',
                        marginTop: 30
                    }} >
                        <SimpleText>Add</SimpleText>
                    </SubmitButton>
                    <TouchableOpacity onPress={this.props.goBack}>
                        <SimpleText style={{ color: '#38b6ff' }}>Go back</SimpleText>
                    </TouchableOpacity>
                </Container>
            </ImageBackground>
        )
    }
}
export default AddWalletPage