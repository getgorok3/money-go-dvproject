import React, { Component } from 'react';
import { View, Text, TextInput, Button, TouchableOpacity, FlatList } from 'react-native';



class EditWalletPage extends Component {

    state = {
        today: new Date(),
        index: null,
        walletName: '',
        money: 0,
        newAmountOfMoney: '',
        topic: '',
        type: '',
        transaction: []
    }


    componentDidMount() {
        this.getDefaultValue()


    }
    getDefaultValue = () => {
        const { item } = this.props
        this.setState({ ...item })

    }
    changeValue = (state, value) => this.setState({ [state]: value })

    onAddLedger = () => {
        let newValue = parseInt(this.state.money, 10)

        const amountOfMoney = parseInt(this.state.newAmountOfMoney, 10)
        if (this.state.type === 'i') {
            newValue = newValue + amountOfMoney
        } else {
            newValue = newValue - amountOfMoney
        }
        console.log('you have: ', newValue)
        this.setState({
            money: newValue,
            transaction: {
                topic: this.state.topic,
                type: this.state.type,
                newAmountOfMoney: this.state.newAmountOfMoney,
                date: new Date().toDateString(),
                walletName: this.state.walletName,
                money: newValue
            }
        }, () => {
            this.props.onSave(this.state.index, this.state.walletName, this.state.money, this.state.transaction)
        })
        // this.setState({  })


        // this.props.goBack()
    }

    goBack = () => {
        const { goBack } = this.props
        console.log("go go ",this.props.transaction);
        // this.props.transaction.forEach(element => {
        //     console.log('element w name: ', element.transaction.walletName)
        // });
        const data = ()=>{
            return this.props.transaction.filter(data => data.transaction.walletName == this.state.walletName );
        }
        console.log("data",this.props.transaction);
        goBack()
    }

    renderItem = ({ item, index }) => {
        return (
            <Text >Topic: {item.topic}  tpye: {item.type}    money: {item.newAmountOfMoney}
                date: {item.date} wallet: {item.walletName} money left: {item.money} </Text>
        )
    }
    render() {

        return (
            <View style={{ marginLeft: '20%' }}>

                <Text>Keep a ledger</Text>
                <Text>{this.state.walletName}</Text>
                <TextInput placeholderTextColor='#cccccc' clear placeholder='Add Topic' value={this.state.topic}
                    onChangeText={value => this.changeValue('topic', value)} ></TextInput>

                <TextInput placeholderTextColor='#cccccc' clear placeholder='Type: income or payment'
                    value={this.state.type}
                    onChangeText={value => this.changeValue('type', value)} ></TextInput>

                <TextInput placeholderTextColor='#cccccc' clear placeholder='input amount of money'
                    value={this.state.newAmountOfMoney}
                    onChangeText={value => this.changeValue('newAmountOfMoney', value)} ></TextInput>

                <Button onPress={this.onAddLedger} title="Add ledger"> </Button>
                <Button onPress={this.goBack} title="Goback"> </Button>

                {this.state.transaction !== [] ? (
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.transaction}
                        extraData={this.state}
                        renderItem={this.renderItem}
                    />
                ) : (
                        <View></View>
                    )}


            </View>
        )
    }
}
export default EditWalletPage