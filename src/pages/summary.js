import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ImageBackground, ScrollView } from 'react-native';
import { images } from "../utilities";
import {
    Container2, Title, SubmitButton, SubTitle, Column, Row,
    SimpleText
} from "../components/General.styled";
import { Card, WhiteSpace, WingBlank, Icon, Button, SegmentedControl, NoticeBar } from '@ant-design/react-native';
import Footer from '../components/footer'

class SummaryPage extends Component {
    state = {
        wallet: [],
        date: new Date().toDateString(),
        todayTransaction: [],
        inCome: 0,
        expenses: 0,
        isSelectToday: true
    }
    onSelectValueChange = (value) => {
        this.setState({ isSelectToday: !this.state.isSelectToday })
    }

    componentDidMount = () => {
        let inCValue = 0
        let exPValue = 0
        console.log('YAKKKKKK', this.props.wallet)
        let data = this.props.transaction
        if (data !== []) {
            data = data.filter(function (item) {
                return item.transaction.date == new Date().toDateString()
            })
            inCValue = this.getIncome(data)
            exPValue = this.getExpenses(data)
        }
        this.setState({
            wallet: this.props.wallet,
            todayTransaction: data,
            inCome: inCValue,
            expenses: exPValue
        })
    }

    getIncome = (data) => {
        let inComeData = []
        let income = 0
        inComeData = data.filter(function (item) {
            return item.transaction.type == 'i'
        })
        inComeData.forEach(element => {
            income = income + parseFloat(element.transaction.newAmountOfMoney)
        });
        return income
    }
    getExpenses = (data) => {
        let exPensesData = []
        let exPenses = 0
        exPensesData = data.filter(function (item) {
            return item.transaction.type == 'e'
        })
        exPensesData.forEach(element => {
            exPenses = exPenses + parseFloat(element.transaction.newAmountOfMoney)
        });
        return exPenses
    }

    getBalance = () => {
        let value = 0
        this.state.wallet.forEach(element => {
            value = value + parseFloat(element.money)
        });
        if (value == NaN) {
            return 0
        } else {
            return value
        }

    }

    renderTodayItem = ({ item, index }) => {
        console.log('Itemssss', item)
        return (

            <Card full style={{ marginBottom: 20 }}>
                <Card.Header
                    title={item.transaction.topic}
                    extra={this.state.date}
                />
                <Card.Body>
                    <View style={{ height: 60, }}>
                        <Text style={{ marginLeft: 16 }}> Wallet:  {item.transaction.walletName}</Text>
                        <Text style={{ marginLeft: 16 }}> Type: {item.transaction.type} </Text>
                        <Text style={{ marginLeft: 16 }}>the amount of money:  {item.transaction.newAmountOfMoney}</Text>
                        <Text style={{ marginLeft: 16 }}> Balance: {item.transaction.money}</Text>

                    </View>
                </Card.Body>
            </Card>

        )
    }




    render() {
        console.log('Today : ', this.props.transaction)
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg}>

                <Title >Money Go</Title>
                
                <Container2 style={{ flex: 1, }} >

                    <ScrollView  >
                        <WingBlank size="lg">
                            <SubTitle style={{ color: '#162730', fontWeight: 'bold' }}>Summary</SubTitle>
                            {/* <Card style={{ marginTop: 20, borderColor: 'transparent' }}>
                                <Card.Body>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center',overflow:'hidden' }}>
                                        <Column >
                                            <SimpleText style={{ color: '#00b359' }}>Income  </SimpleText>
                                            <Row>
                                                <SimpleText style={{ color: '#00b359' }}>
                                                    <Icon name="arrow-up" style={{ color: '#00b359' }}></Icon>
                                                    {this.state.inCome}</SimpleText>
                                            </Row>
                                        </Column>
                                        <Column >
                                            <SimpleText style={{ color: '#cc0000' }}>Expenses  </SimpleText>
                                            <Row>
                                                <SimpleText style={{ color: '#cc0000' }}>
                                                    <Icon name="arrow-down" style={{ color: '#cc0000' }}></Icon>
                                                    {this.state.expenses}</SimpleText>
                                            </Row>
                                        </Column>

                                        <Column >
                                            <SimpleText style={{ color: '#ffcc00' }}>Balance  </SimpleText>
                                            <Row>
                                                <SimpleText style={{ color: '#ffcc00' }}>
                                                    <Icon name="arrow-down" style={{ color: '#ffcc00' }}></Icon>
                                                    {this.getBalance()}</SimpleText>
                                            </Row>
                                        </Column>

                                    </View>
                                </Card.Body>
                            </Card> */}
                            <Card style={{ backgroundColor: '#80ffaa',borderColor: 'white' }}>
                                <Card.Header
                                    title={'Income'}
                                    thumbStyle={{ marginRight: '5%' }}
                                    extra={this.state.inCome}
                                />
                            </Card>
                            <Card style={{ backgroundColor: '#ffb3b3' ,borderColor: 'white'}}>
                                <Card.Header
                                    title={'Expenses'}
                                    thumbStyle={{ marginRight: '5%' }}
                                    extra={this.state.expenses}
                                />
                            </Card>
                            <Card style={{ backgroundColor: '#ffd699',borderColor: 'white' }}>
                                <Card.Header
                                    title={'Balance'}
                                    thumbStyle={{ marginRight: '5%' }}
                                    extra={this.getBalance()}
                                />
                            </Card>

                        </WingBlank>

                        <WingBlank size="lg">
                        <WhiteSpace></WhiteSpace>
                            <SegmentedControl
                                selectedIndex={0}
                                values={["Today 's payment", 'All transactions']}
                                tintColor={'#cb6ce6'}
                                onValueChange={this.onSelectValueChange}
                                style={{ height: 40 }}
                            />
                            <WhiteSpace />
                            {this.state.isSelectToday === true && this.state.todayTransaction && this.state.todayTransaction !== [] ? (
                                    <FlatList
                                        keyExtractor={(item, index) => index.toString()}
                                        data={this.state.todayTransaction}
                                        renderItem={this.renderTodayItem}
                                    />
                                ) : (
                                    <FlatList
                                        keyExtractor={(item, index) => index.toString()}
                                        data={this.props.transaction}
                                        renderItem={this.renderTodayItem}
                                    />
                                )}

                        </WingBlank>
                    </ScrollView>

                </Container2>
                <Footer ></Footer>
            </ImageBackground>
        )
    }
}
export default SummaryPage