import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ImageBackground, ScrollView } from 'react-native';
import { images } from "../utilities";
import {
    SimpleContainer, Title, SubmitButton, Container3, IconFrame, FullWImage,
    SimpleText,
    SubTitle,
    Column
} from "../components/General.styled";
import { Card, WhiteSpace, WingBlank, Icon, Badge, List } from '@ant-design/react-native';
import Footer from '../components/footer'


class WalletPages extends Component {
    state = {
        wallet: [],
    }
    componentDidMount = () => {
        this.setState({
            wallet: this.props.wallet
        })
    }
    goToWalletDetail = (item, index) => {
        const { goToWalletDetail } = this.props
        let sentItem = {
            index: index,
            walletName: item.walletName,
            money: item.money
        }
        this.setState({ wallet: [] })
        goToWalletDetail(sentItem)

    }


    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.goToWalletDetail(item, index)}>

                <Card full>
                    <Card.Header
                        extra={item.money}
                        title={item.walletName}
                        thumb={<IconFrame style={{ marginRight: 20, marginTop: 5, marginBottom: 5 }}>
                            <FullWImage source={images.wallet1} ></FullWImage>
                        </IconFrame>}
                    >
                    </Card.Header>
                </Card>

            </TouchableOpacity >

        )
    }
    render() {

        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg}>


                <SimpleContainer style={{
                    backgroundColor: 'white', marginTop: '15%', marginBottom: '5%',
                    marginRight: '5%', marginLeft: '5%'
                }} >

                    <SubTitle style={{ color: 'black', marginTop: 20, marginBottom: 20, fontWeight: 'bold' }}>Your wallets</SubTitle>

                    <WhiteSpace />


                    <ScrollView style={{ flex: 1 }} >
                        <WingBlank size="lg">
                            <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                data={this.state.wallet}
                                renderItem={this.renderItem}
                            />

                        </WingBlank>

                    </ScrollView>

                    <View style={{
                        justifyContent: 'flex-end', alignItems: 'center', padddingTop: 30,
                        marginTop: 10
                    }}>
                        <SubmitButton onPress={this.props.goToAddWallet}
                            style={{ backgroundColor: '#3399ff' }}>

                            <SimpleText>Add +</SimpleText>

                        </SubmitButton>
                    </View>

                </SimpleContainer>

                <Footer ></Footer>
            </ImageBackground>

        )
    }
}
export default WalletPages