import React, { Component } from 'react';
import { ImageBackground, TouchableOpacity, Text, View } from 'react-native';
import axios from 'axios';
import {
    Container2, Container, LogoFrame, LoginForm, FullWImage, UserInput, SubmitButton,
    SimpleText, Container4,
} from "../components/General.styled";
import { images } from "../utilities";
import { WhiteSpace, } from '@ant-design/react-native';
import Footer from '../components/footer'


class ProfilePage extends Component {
    state = {
        username: '',
        password: '',
        firstName: '',
        lastName: '',
        token: '',
        newPassword: ''
    }
    componentDidMount = () => {
        this.getDefaultValue()
    }
    getDefaultValue = () => {

        this.setState({
            username: this.props.username,
            password: this.props.password,
            firstName: this.props.firstName,
            lastName: this.props.lastName,
            token: this.props.token,
        })
    }
    changeValue = (state, value) => this.setState({ [state]: value }) 
    onSentReqChanePassword = () => {
        axios({
            url: 'https://zenon.onthewifi.com/moneyGo/users/password',
            method: 'put',
            headers: {
                Authorization : `Bearer ${this.props.token}`
            },
            data: {
                oldPassword: this.props.password,
                newPassword: this.state.newPassword
            },
            
        }).then(res => {
            console.log('response', res)
        }).catch( e =>{
            console.log('error', e)
        })

    }
    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={images.loginForm}>
                <View style={{ flex: 1, }} >
                    <LogoFrame style={{
                        marginLeft: '25%', marginRight: '25%'
                    }}><FullWImage style={{ borderRadius: 120 }} source={images.logo} /></LogoFrame>
                    <Container4 >

                        <SimpleText style={{ color: '#ffcc00', fontWeight: 'bold' }}>Profile</SimpleText>
                        <SimpleText style={{ color: '#ffcc00', fontWeight: 'bold' }}>Welcome: {this.state.username}</SimpleText>
                        <WhiteSpace />

                        <UserInput placeholderTextColor='#cccccc' clear placeholder='New Password'
                            value={this.state.newPassword}
                            style={{color: 'black'}}
                            onChangeText={value => this.changeValue('newPassword', value)} ></UserInput>
                        <WhiteSpace />
                        <SubmitButton style={{ marginLeft: '20%', marginRight: '20%', width: '60%' }}
                        onPress={this.onSentReqChanePassword}>
                            <Text style={{ textAlign: 'center' }}>Change password</Text>
                        </SubmitButton>

                    </Container4>
                </View>
                <Footer ></Footer>
            </ImageBackground >
        )
    }

}
export default ProfilePage