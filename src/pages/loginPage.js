// Log in page
import React, { Component } from 'react';
import { ImageBackground, TouchableOpacity, Text, } from 'react-native';
import axios from 'axios';
import { Container, LogoFrame, LoginForm, FullWImage, UserInput, SubmitButton,
    SimpleText } from "../components/General.styled";
import { images } from "../utilities";
import { WhiteSpace, } from '@ant-design/react-native';
class LoginPage extends Component {
    state = {
        username: 'Gg@gmail.com',
        password: '123456',
        firstName: '',
        lastName: '',
        token: ''
    }
    changeValue = (state, value) => this.setState({ [state]: value })       //keep data from user to state

    onPressLogIn = () => {
        axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
            email: this.state.username, password: this.state.password
        }).then((response) => {
            console.log('Login success: ', response);
            this.setState({
                token: response.data.user.token, firstname: response.data.user.firstName,
                lastname: response.data.user.lastName
            })
            this.props.onSuccess(this.state)
            this.props.goSummary()

        }).catch(function (error) {
            console.log('error: ', error.response.data.errors)
        });


    }

    componentDidMount = () => {
        console.log('Props: ', this.props)
    }

    render() {
        console.log('Props: ', this.props)
        return (
            <ImageBackground style={{ flex: 1 }} source={images.loginForm}>
                <Container >
                    <LogoFrame><FullWImage style={{ borderRadius: 120 }} source={images.logo} /></LogoFrame>

                    <LoginForm>

                        <UserInput placeholderTextColor='#cccccc' clear placeholder='Username' value={this.state.username}
                            onChangeText={value => this.changeValue('username', value)} ></UserInput>

                        <UserInput placeholderTextColor='#cccccc' clear placeholder='Password' value={this.state.password}
                            onChangeText={value => this.changeValue('password', value)} ></UserInput>

                        <SubmitButton onPress={this.onPressLogIn}
                            style={{ marginRight: '30%', marginLeft: '30%', marginTop: 40, }}>
                            <SimpleText style={{ color: '#ffcc00', fontWeight: 'bold' }}>Login</SimpleText>
                        </SubmitButton>
                        <WhiteSpace></WhiteSpace>
                        <TouchableOpacity onPress={this.props.goToRegister}>
                            <SimpleText >Register</SimpleText>
                        </TouchableOpacity>

                    </LoginForm>

                </Container>
            </ImageBackground>

        )
    }
}
export default LoginPage