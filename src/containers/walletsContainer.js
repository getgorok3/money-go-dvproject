import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import {ROUTE_TO_ADD_WALLET,ROUTE_TO_EDIT_WALLET ,ROUTE_TO_WALLET_DETAIL} from "../constants/routeConstant.js";
import WalletPage from "../pages/walletsPage";

const mapStateToProps = ({ wallet, historyOfTransacton }) => ({
    wallet,
    transaction: historyOfTransacton,
})

const mapDispatchToProps = dispatch => ({
    goToAddWallet: () => {
        dispatch(push(ROUTE_TO_ADD_WALLET))
    },
    goToEditWallet: item =>{
        dispatch(push({pathname: ROUTE_TO_EDIT_WALLET, state: { ...item }}))
    },
    goToWalletDetail: item =>{
        dispatch(push({pathname: ROUTE_TO_WALLET_DETAIL, state: { ...item }}))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WalletPage)