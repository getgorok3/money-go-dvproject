import { connect } from 'react-redux'
import { goBack,push } from 'connected-react-router'
import AddWalletPage from "../pages/addWalletPage";
import {addMoreWallet} from '../actions/appActions'
import {ROUTE_TO_WALLTETS} from '../constants/routeConstant'

const mapStateToProps = ({ wallet }) => ({
    walletName: wallet.walletName,
    money: wallet.money,
})

const mapDispatchToProps = dispatch => ({
    addTheWallet: newWallet => {
        dispatch(addMoreWallet(newWallet))
        dispatch(goBack())
    },
    goToSummary: () => {
        dispatch(push(ROUTE_TO_WALLTETS))
    },
    goBack: () => {
        dispatch(goBack())
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddWalletPage)