import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import {ROUTE_TO_ADD_WALLET,ROUTE_TO_EDIT_WALLET,ROUTE_TO_WALLTETS } from "../constants/routeConstant.js";
import SummaryPage from "../pages/summary";

const mapStateToProps = ({ wallet, historyOfTransacton }) => ({
    wallet,
    transaction: historyOfTransacton,
})

const mapDispatchToProps = dispatch => ({
    goToWallets: () =>{
        dispatch(push(ROUTE_TO_WALLTETS)) 
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SummaryPage)