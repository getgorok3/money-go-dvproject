// export up containers link with page

export { default as LoginPage } from './loginContainers'
export { default as RegisterPage } from './registerContainers'
export { default as AddWalletPage } from './addWalletContainer'
export { default as SummaryPage } from './summaryContainer'
export { default as EditWalletPage } from './editWalletContainer'
export { default as WalletsPage } from './walletsContainer'
export { default as ProfilePage } from './profileContainer'
export { default as WalletDetailPage } from './walletDetailContainer'