import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import EditWalletPage from "../pages/editWalletPage";
import { editMoreWallet, addTransactionHistory } from "../actions/appActions";

const mapStateToProps = ({ router,historyOfTransacton }) => ({
    item: router.location.state,
    transaction: historyOfTransacton,
})
const mapDispatchToProps = dispatch => ({
    onSave: (index, walletName, money, transaction) => {
        dispatch(editMoreWallet(index, {walletName,money }))
        dispatch(addTransactionHistory({  transaction }))
    },
    goBack: () => {
        dispatch(goBack())
    },
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditWalletPage)