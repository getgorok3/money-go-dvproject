import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import {ROUTE_TO_REGISTER, ROUTE_TO_SUMMARY } from "../constants/routeConstant.js";
import LoginPage from "../pages/loginPage";
import { saveCurrentUser } from "../actions/appActions";

const mapStateToProps = ({ userAccount }) => ({
    username: userAccount.username,
    password: userAccount.password,
    firstname: userAccount.firstname,
    lastname: userAccount.lastname,
    token: userAccount.token
})

const mapDispatchToProps = dispatch => ({
    onSuccess: user => {
        dispatch(saveCurrentUser(user))
    },
    goToRegister: ()=> {
        dispatch(push(ROUTE_TO_REGISTER))
    },
    goSummary: () =>{
        dispatch(push(ROUTE_TO_SUMMARY))
    }

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage)