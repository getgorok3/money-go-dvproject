import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import WalletDetailPage from "../pages/walletDetail";
import { editMoreWallet } from "../actions/appActions";
import {ROUTE_TO_EDIT_WALLET } from "../constants/routeConstant.js";

const mapStateToProps = ({ router,historyOfTransacton }) => ({
    item: router.location.state,
    transaction: historyOfTransacton,
})
const mapDispatchToProps = dispatch => ({
    onSave: (index, walletName, money, transaction) => {
        dispatch(editMoreWallet(index, {walletName,money }))
    },
    goBack: () => {
        dispatch(goBack())
    },
    goToEditWallet: item =>{
        dispatch(push({pathname: ROUTE_TO_EDIT_WALLET, state: { ...item }}))
    }
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WalletDetailPage)