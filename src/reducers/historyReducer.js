import { HISTORY_ADD, WALLET_EDIT } from "../constants/appConstants";

const addTransactionHistory = (state, payload) => {
    const mapped = state.map(item => item)
    mapped.push(payload)
    return mapped
}



export default (state = [], { type, payload }) => {
    switch (type) {
        case HISTORY_ADD:
            return addTransactionHistory(state, payload)
        default:
            return state
    }
}
// simple add and config logic in the class


