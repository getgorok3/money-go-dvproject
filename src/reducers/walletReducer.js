import { WALLET_ADD, WALLET_EDIT } from "../constants/appConstants";

const addWallet = (state, payload) => {
    const mapped = state.map(item => item)
    mapped.push(payload)
    return mapped
}
const editWallet = (state, { index, ...rest }) => {
    return state.map((item, targetIndex) =>
        targetIndex === index ? rest : item
    )
}

export default (state = [], { type, payload }) => {
    switch (type) {
        case WALLET_ADD:
            return addWallet(state, payload)
        case WALLET_EDIT:
            return editWallet(state, payload)
        default:
            return state
    }
}

