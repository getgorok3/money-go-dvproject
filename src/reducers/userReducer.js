// Reducer for managing user

import { SAVE_CURRENT_USER } from '../constants/appConstants';



export default (state = [], { type, payload }) => {
    switch (type) {
        case SAVE_CURRENT_USER:                         //: add the user to redux
            return { ...state, ...payload }
        default:
            return state
    }
}