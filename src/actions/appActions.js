// Define action that used in redux
import { SAVE_CURRENT_USER, WALLET_ADD, WALLET_EDIT, HISTORY_ADD, } from '../constants/appConstants'

// ---------- User actions  ----------
export const saveCurrentUser = ({ username, password, firstname, lastname, token }) => ({
    type: SAVE_CURRENT_USER,
    payload: { username, password, firstname, lastname, token },
})
// ---------- Wallet actions ----------
export const addMoreWallet = ({ walletName, money }) => ({
    type: WALLET_ADD,
    payload: { walletName, money },
})

export const editMoreWallet = (index, { walletName, money }) => ({
    type: WALLET_EDIT,
    payload: { index, walletName, money },
})

export const addTransactionHistory = ({  transaction }) => ({
    type: HISTORY_ADD,
    payload: {  transaction },
})


