import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import userReducer from "../reducers/userReducer";
import walletReducer from '../reducers/walletReducer'
import historyReducer from '../reducers/historyReducer'

const reducers = history =>
    combineReducers({
        userAccount: userReducer,
        wallet: walletReducer,
        historyOfTransacton: historyReducer,
        router: connectRouter(history)
    })

export default reducers