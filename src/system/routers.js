// Routers 

import React, { Component } from 'react'
import { Route, Switch } from 'react-router-native'
import { ROUTE_TO_LOGIN, ROUTE_TO_REGISTER, ROUTE_TO_ADD_WALLET,ROUTE_TO_SUMMARY,
    ROUTE_TO_EDIT_WALLET,ROUTE_TO_WALLTETS,ROUTE_TO_PROFILE,ROUTE_TO_WALLET_DETAIL
 } from "../constants/routeConstant";
import { LoginPage, RegisterPage, AddWalletPage,SummaryPage,EditWalletPage,
    WalletsPage ,ProfilePage, WalletDetailPage} from "../containers/index.js";


export default class Router extends Component {
    render() {
        return (
            <Switch>
                <Route exact path={ROUTE_TO_LOGIN} component={LoginPage} />
                <Route exact path={ROUTE_TO_REGISTER} component={RegisterPage} />
                <Route exact path={ROUTE_TO_ADD_WALLET} component={AddWalletPage} />
                <Route exact path={ROUTE_TO_SUMMARY} component={SummaryPage} />
                <Route exact path={ROUTE_TO_EDIT_WALLET} component={EditWalletPage} />
                <Route exact path={ROUTE_TO_WALLTETS} component={WalletsPage} />
                <Route exact path={ROUTE_TO_PROFILE} component={ProfilePage} />
                <Route exact path={ROUTE_TO_WALLET_DETAIL} component={WalletDetailPage} />
            </Switch>
        )
    }
}